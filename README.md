# Cloutman Keyboard Layout
The Cloutman Keyboard Layout is a custom ergonomic keyboard layout has the following design considerations:

- This is an English language keyboard
- Letter keys are primarilly arranged to conform with letter frequency
- Vowels are all on the home row and all vowels are placed so that they are under the user's fingers in the inital position
- Letters that are commonly used together are grouped together
- Location of or the finger used to type a character in the QWERTY layout played a role in placing letters. Less used letters in hard to reach parts of the keyboard were left in place. No searching for X or Z
- Alphabetical order was given some consideration
- Punctuation is king. Numbers are second class citizens
- This is a keyboard for a computer, not a typewriter. Usage in programming and technical operations was given consideration
- The caps lock key puts the layout into "accountant mode" where numbers and mathematical operators are given precedence over linguistic symbols
- Support for OS X is included. Option + shift key macros maybe appear on some designs. However, the Apple logo and any Apple trademarks have been left out of keyboard designs intentionally. 

![Cloutman Keyboard Layout](cloutman-layout.png)

This layout is designed to be easy for QWERTY users to learn. However, if you are used to QWERTY,
you may find yourself overreaching before you master touch typing on the Cloutman Layout. Try to
relax into the experience. Many common English language words can be typed on the home row. "I"
is directly below your left index finger and "the" is under the first three fingers of your right
hand. The apostrophe is next to the 's', which is next to the 'e'. Things that go at the end are of a
word, phrase or sentence, mostly next to your right pinky. The comma and the double quote share a key
for the convinience of writing English language quotations. Most words do not require you to strain 
to type them. Your hands will move very little compared to typing on QWETY. You may discover some
bad typing habits from previous QWERTY usage. But, you may be amazed how comfortable typing can be on
a layout designed for humans, not typewriters.

## Project Background
I've always hated the QWERTY layout. I remember looking at it as a child and thinking, 
who thought that was a good idea? Eventually, I learned about the Dvorak layout and was
eager to try it for many years, but physical Dvorak keyboards were rare and expensive.
Additionally,keymapping was a non-trivial problem on the operating systems used in the 
1990s. Later, the operating system software and began offering better support for
international keyboards and non-QWERTY English language layouts. I tried to make the 
conversion to Dvorak in 2005, but gave up in frustration. The ideas behind the layout are
pretty good, but some things about it didn't hit the sweet spot for me. Thus, I found it 
as painful to learn as QWERTY had been. 

Dvorak makes more logical sense because it was built for ergonomics while QWERTY's primary
design consideration was to keep mechanical keyboards from jamming when used at a fast
clip. But not having all the vowels under my fingers was problematic. One shouldn't need 
to reach to type the word 'I'. And, because no consideration was given to easing the transition
from QWERTY, learning the locations of rarely used letters, like X, is more difficult than need be.

So, over a decade later, keyboard customization and manufacture is finally hitting a point
where one can order a customized physical device, install a file in most operating systems,
and hit the ground running with whatever layout suits their needs. The Cloutman Layout
is designed to suit mine, as a computer programmer. Perhaps it might be useful to you as
well.

This repository contains designs for Cloutman Layout keyboards and key mapping files for
diferent common operating systems. While there is no plug as play solution for supporting the
layout at the hardware level, yet, the goal of this project is to make installing the Cloutman
Layout on any modern operating system relatively painless and building or ordering a high quality
keyboard with the keys in the correct location less painful.


